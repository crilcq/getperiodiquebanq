class NoticeBAnQ:
    def __init__(self, json_data):
        self.titre = json_data["titre"]
        self.annee = json_data["annee"]
        self.mois = json_data["mois"]
        self.jour = json_data["jour"]
        self.bitstreams = json_data["bitstreams"]
        self.pdfs = []
        if self.bitstreams is not None and self.bitstreams["pdf"] is True:
            self.pdfs = self.bitstreams["liste"]
