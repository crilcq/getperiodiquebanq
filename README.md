# getPeriodiqueBAnQ
Ce script nécessite l'installation de MuPDF si on désire extraire le texte du PDF dans un fichier texte. Voir : https://github.com/pymupdf/PyMuPDF

Le paramètre « handle » est le ARK d'une livraison du périodique ciblé. Ça peut être n'importe laquelle.

## Exemple de requête
Pour télécharger le Franc-Parleur à partir du ARK de cette livraison https://numerique.banq.qc.ca/patrimoine/details/52327/4307017 :
```
python3 main.py --handle=52327/4307017 --download_path=/home/olivier/journaux/franc-parler --extraction_texte=true --annee_debut=1879 --annee_fin=1879
```