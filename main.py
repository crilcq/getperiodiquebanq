import argparse
import io
import os
import time
import unicodedata

import fitz
import requests
from pathvalidate import sanitize_filename
from requests.adapters import HTTPAdapter
from urllib3 import Retry

from notice_banq import NoticeBAnQ

parser = argparse.ArgumentParser()
parser.add_argument('--handle', type=str, required=True)
parser.add_argument('--download_path', type=str, required=True)
parser.add_argument('--extraction_texte', type=bool, required=True)
parser.add_argument('--annee_debut', type=str, required=True)
parser.add_argument('--annee_fin', type=str, required=True)
parser.add_argument("--mois_debut", type=str, required=False)
parser.add_argument("--jour_debut", type=str, required=False)
args = parser.parse_args()
handle = args.handle
download_path = args.download_path.rstrip("/")
extraction_texte = args.extraction_texte
annee_debut = args.annee_debut
annee_fin = args.annee_fin
jour_debut = args.jour_debut
mois_debut = args.mois_debut
headers = {"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
           "Accept-Encoding": "gzip, deflate, br,zstd",
           "Cache-Control": "no-cache",
           "Pragma": "no-cache",
           "Priority": "u=0,i",
           "Sec-Ch-Ua": '"Not/A)Brand";v="8", "Chromium";v="126", "Google Chrome";v="126"',
           "Sec-Ch-Ua-Platform": '"macOS"',
           "Sec-Fetch-Dest": "document",
           "Sec-Fetch-Mode": "navigate",
           "Sec-Fetch-Site": "none",
           "Accept-Language": "fr-CA,fr;q=0.9,en-CA;q=0.8,en;q=0.7,en-GB;q=0.6,en-US;q=0.5,it;q=0.4",
           "Cookie": "__ssds=3; __ssuzjsr3=a9be0cd8e; __uzmaj3=c155f9af-dcc1-4cb5-a66d-a04ddec905aa; __uzmbj3=1717506621; _ga_3DY3YB33EB=GS1.1.1719712608.3.0.1719712611.57.0.0; __uzmb=1719712630; __uzma=637daaa4-27a2-4b84-89bc-69573b0869e6; __uzme=0348; _gid=GA1.3.373901545.1719886162; _clck=1ccfajw%7C2%7Cfn4%7C0%7C1609; banq_lang_c=; _ga_5DG3602LLS=GS1.1.1719887588.1.0.1719887592.56.0.0; _ga_H2DD3LLSP3=GS1.1.1719924396.2.0.1719924396.60.0.0; __uzmlj3=Mi2xFfcbpLYl119Cl7xIc9EFCmz9MzImsRjc6OrKE5Y=; _ga=GA1.1.153222345.1716864594; __uzmcj3=179239432671; __uzmdj3=1719928080; __uzmfj3=7f60004b041fb4-f89f-42c8-b8f1-4650608613e317175066215502421459260-57e2a6514c62487194; uzmxj=7f9000e1da6051-cbb4-4925-a5ad-7b6e4b4575d65-17175066215502421459260-eddd5a5ae905f4be94; _clsk=1iik2hw%7C1719929091052%7C23%7C1%7Cj.clarity.ms%2Fcollect; _ga_4HRTYM07SM=GS1.1.1719925273.10.1.1719929091.54.0.0; _ga_5MMSQ5Q86S=GS1.1.1719924396.12.1.1719929091.54.0.0; JSESSIONID=543DA29E8D820759225DE8D9CB745454; __uzmc=5983726548154; __uzmd=1719937881; __uzmf=7f60004e5ed7b9-4844-4ce6-9047-0806318f53d21719712630675225250575-2c2473f9b39c7c48265; uzmx=7f9000e1da6051-cbb4-4925-a5ad-7b6e4b4575d67-17168645929423073288308-5ff4bc76b577f9ac898",
           "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36"}

url_service_notice = "https://collections.banq.qc.ca/api/service-notice"
url_service_periodiques = "https://collections.banq.qc.ca/api/service-periodique"

s = requests.Session()

retries = Retry(total=15,
                backoff_factor=20,
                status_forcelist=[104, 500, 502, 503, 504])

s.mount('https://', HTTPAdapter(max_retries=retries))


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])


def get_filename_from_notice(notice):
    fname = notice.titre + "_" + str(notice.annee) + "_" + str(notice.mois).zfill(2) + "_" + str(notice.jour).zfill(2)
    return sanitize_filename(fname).replace(" ", "_").replace(",", "_")


def get_filename_from_pdf_object(pdfObject):
    filenameComplet = pdfObject["fichier"]
    filename = filenameComplet[filenameComplet.rfind("/") + 1:]
    return sanitize_filename(filename).replace(" ", "_").replace(",", "_")


def create_and_get_path_from_notice(notice):
    path = download_path + "/" + str(notice.annee)
    if (notice.mois is not None):
        path += "/" + str(notice.mois).zfill(2)
        if (notice.jour is not None):
            path += "/" + str(notice.jour).zfill(2)
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def create_text_file_from_pdf(nomFichier):
    try:
        with fitz.open(nomFichier) as doc:
            text = u""
            i = 1
            for page in doc:
                text += u"\n###PAGE###" + str(i) + "###" + "\n" + page.getText()
                i = i + 1
        nom_fichier_output = nomFichier.replace(".pdf", ".txt")
        text_utf8 = text.replace("\u0000", "").encode("UTF-8", "strict")
        output_file = io.open(nom_fichier_output, "w", encoding="UTF-8")
        output_file.write(text_utf8.decode("UTF-8", 'strict'))
        output_file.close()
    except:
        print("Erreur au moment de la création du fichier texte à partir du PDF " + nomFichier)


def telechargement_pdfs_notice(notice):
    if len(notice.pdfs) == 1:
        path = create_and_get_path_from_notice(notice)
        filename = get_filename_from_pdf_object(notice.pdfs[0])
        exists = os.path.exists(path + "/" + filename)
        if not exists:
            r = s.get(notice.pdfs[0]["url"], allow_redirects=True, timeout=10, headers=headers)
            open(path + "/" + filename, 'wb').write(r.content)
            if extraction_texte:
                try:
                    create_text_file_from_pdf(path + "/" + filename)
                except:
                    print("Une erreur est survenue dans le cadre de la création du fichier texte du PDF : " + filename)
        else:
            print("Existe déjà!" + path + "/" + filename)
    elif len(notice.pdfs) > 1:
        for idx, pdfObject in notice.pdfs:
            filename = get_filename_from_pdf_object(pdfObject)
            path = create_and_get_path_from_notice(notice)
            exists = os.path.exists(path + "/" + filename)
            if not exists:
                r = s.get(pdfObject["url"], allow_redirects=True, timeout=10, headers=headers)
                open(path + "/" + filename, 'wb').write(r.content)
                if extraction_texte:
                    try:
                        create_text_file_from_pdf(path + "/" + filename)
                    except:
                        print(
                            "Une erreur est survenue dans le cadre de la création du fichier texte du PDF : " + filename)
            else:
                print("Existe déjà!" + path + "/" + filename)


def get_url_annee_periodique(annee):
    return url_service_periodiques + "?handle=" + handle + "&annee=" + str(annee)


def traitement_jour(jour):
    time.sleep(2)
    handle_jour = jour["handle"]
    if handle_jour is not None:
        notice_response = s.get(url_service_notice + "?handle=" + handle_jour, timeout=10, headers=headers)
        if notice_response.status_code == 200 and notice_response.json() is not None:
            notice = NoticeBAnQ(notice_response.json())
            if notice.pdfs is not None and len(notice.pdfs) >= 1:
                telechargement_pdfs_notice(notice)
            else:
                print("Aucun PDF!")
        else:
            print("Erreur au niveau de la réponse du serveur pour ce jour \n" + "(" + jour + ")" + str(
                notice_response.status_code))


def traitement_mois(mois, annee):
    jours = mois["jours"]
    mois_a_traiter = mois["mois"]
    if (jours is not None):
        for jour in jours:
            jour_a_traiter = jour["jour"]
            if (jour_debut is not None and jour_debut != "" and mois_debut is not None and mois_debut != ""
                    and mois_a_traiter == int(mois_debut) and str(annee) == str(annee_debut)):
                if mois_a_traiter == int(mois_debut) and jour_a_traiter >= int(jour_debut):
                    traitement_jour(jour)
                else:
                    print("On ignore ce jour : " + str(annee) + "-" + str(mois_a_traiter).zfill(2) + "-" + str(jour_a_traiter).zfill(2))
            else:
                traitement_jour(jour)


def traitementAnnee(annee):
    url = get_url_annee_periodique(annee)
    response_annee = s.get(url, timeout=10, headers=headers)
    if response_annee is not None and response_annee.json() is not None and response_annee.json()["dates"] is not None:
        liste_mois = response_annee.json()["dates"]
        for mois in liste_mois:
            if mois_debut is not None and mois_debut != "" and str(annee) == str(annee_debut):
                mois_a_traiter = mois["mois"]
                if mois_a_traiter >= int(mois_debut):
                    traitement_mois(mois, annee)
                else:
                    print("On ignore ce mois : " + str(mois_a_traiter).zfill(2) + " - " + str(annee))
            else:
              traitement_mois(mois, annee)
            print("Mois traité. On dort 10 secondes...")
            time.sleep(10)
    print("Année traitée, on dort 30 minutes")
    time.sleep(1800)


def main():
    annees_parution_response = s.get(url_service_periodiques + "?handle=" + handle + "&info=annees-parution",
                                     timeout=10,
                                     headers=headers)
    if annees_parution_response.json() is not None and annees_parution_response.json()["annees"] is not None:
        liste_annees = annees_parution_response.json()["annees"]
        for annee in liste_annees:
            if annee_debut is not None and annee_fin is not None:
                if int(annee_debut) <= int(annee) <= int(annee_fin):
                    traitementAnnee(annee)
                    time.sleep(60)
            elif annee_debut is not None:
                if int(annee) >= int(annee_debut):
                    traitementAnnee(annee)
                    time.sleep(60)
            elif annee_fin is not None:
                if int(annee) <= int(annee_fin):
                    traitementAnnee(annee)
                    time.sleep(60)
            else:
                traitementAnnee(annee)
                time.sleep(60)


main()
